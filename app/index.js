var app = new Vue({
    el: "#app",
    data: {
        formData:{
            username:"bob"        
        },
        inputText:'',
        numbers: [-1,3,5,-9,10,4,5]
    },
    methods:{
        statusFromId(id){
            const status = ({
                1:"am",
                2:"man",
                3:"wan"
            })[id]

            return status || "unknown status " + id
        },
        filterPositive(numbers){

            return numbers.filter(function(number){
                return number>=0
            })
        },
        filterPositiveSum(numbers){
            return this.filterPositive(numbers).reduce(function(sum,val){
                return sum+val
            })
        },
        viewNumbers(){
            console.log(this.numbers)
        }
    },
    computed: {
        totalNumber: {
            get:function(){
                return this.filterPositiveSum(this.numbers)
            },
            set:function(newTotal){
                var oldValue=this.totalNumber
                var diff=newTotal-oldValue
                this.numbers.push(diff)
            }
        }
    }
})